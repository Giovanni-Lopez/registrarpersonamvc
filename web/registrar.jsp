<%-- 
    Document   : index
    Created on : 05-24-2021, 10:23:39 PM
    Author     : Alexitho López
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/estilos.css" media="screen" />   
        <script src="https://kit.fontawesome.com/3b11acc02f.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <style>
        .icon6{
               font-size: 40px;            
               color: grey;
               height: 35px;
               width: 70px;
               margin: 0px;
               text-align: center;
        }        
        </style>
        <h1>Registro de personas</h1>
        <br>
        <form action="recibir.do" method="POST">
            
            <input type="text" name="txtDui" value="" class="dui form-control inputPadding" placeholder="Ingresa Dui"/><br><br>
            <input type="text" name="txtApellidos" value="" class="apellidos form-control inputPadding" placeholder="Ingresa Apellidos"/><br><br>
            <input type="text" name="txtNombres" value="" class="nombres form-control inputPadding" placeholder="Ingresa Nombres"/><br><br>
            
            <center>                
                <input type="submit" value="Registrar Nueva Persona" class="btn btn-primary"/><br><br>                                
            </center>        
        </form>
        
            <center>
                <button class="btn btn-outline-info">                      
                    <a href="index.jsp"><i class="icon6 fas fa-house-user"></i></a>
                </button>
            </center>
        
    
    </body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>

</html>
