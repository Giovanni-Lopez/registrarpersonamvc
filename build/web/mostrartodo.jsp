<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://kit.fontawesome.com/3b11acc02f.js" crossorigin="anonymous"></script>
        <title>JSP Page</title>
    </head>
    <body>
        <style>
        .icon3{
            font-size: 40px;            
            color: red;
            height: 35px;
            width: 45px;
            margin: 0px;
            text-align: center;
        }
        .icon4{
            font-size: 40px;            
            color: aqua;
            height: 35px;
            width: 45px;
            margin: 0px;
            text-align: center;
        }
        .icon5{
            font-size: 40px;            
            color: grey;
            height: 35px;
            width: 70px;
            margin: 0px;
            text-align: center;
        }
        </style>
        <h1 align="center">Todos los registros!</h1>

        
        <div class="container">
            <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">Dui</th>
                <th scope="col">Apellidos</th>
                <th scope="col">Nombres</th>
                <th scope="col">Eliminar</th>
                <th scope="col">Modificar</th>
              </tr>
            </thead>
            <tbody>
              <c:forEach var="listaTotal" items="${sessionScope.personas}">
                <tr>
                    <td> ${listaTotal.dui}</td>
                    <td>${listaTotal.apellidos}</td>
                    <td>${listaTotal.nombres}</td>
                    <td>
                        <form action="borrar.do" method="post">
                            <input type="hidden" value="${listaTotal.dui}" name="txtEliminar">
                            <button class="btn btn-outline-warning">                      
                                <i class="icon3 fas fa-skull-crossbones"></i>
                            </button>                                
                        </form>
                    </td>
                    <td>
                        <form action="localizar.do" method="post">
                            <input type="hidden" value="${listaTotal.dui}" name="txtEditar">
                            <button class="btn btn-outline-info">                      
                                <i class="icon4 fas fa-check"></i>
                            </button>                            
                        </form>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
          </table>
            
            <button class="btn btn-outline-info">                      
                      <a href="index.jsp"><i class="icon5 fas fa-house-user"></i></a>
            </button>
        </div>
    </body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    
</html>
