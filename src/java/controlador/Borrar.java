package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Persona;

@WebServlet(name = "Borrar", urlPatterns = {"/borrar.do"})
public class Borrar extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

   @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Persona person = new Persona();
        String dui = request.getParameter("txtEliminar");

        try{
            if(person.eliminarDatos(dui) == true){
                request.getRequestDispatcher("exitoborrar.jsp").forward(request, response);

            }else{
                request.getRequestDispatcher("noexitoborarr.jsp").forward(request, response);
            }
        }finally{
            System.out.println("Finalizado");
        }
    }

   @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
