<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : actualizar
    Created on : 05-28-2021, 04:58:46 PM
    Author     : Alexitho López
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            body{
                background-color: #E7E9E9;
            }
        </style>
    </head>
    <body>
        <h1 align="center">Actualizar Registro!</h1><br>
        <div class="container">
            <div class="row">
                <div class="col">
                
                </div>
                <div class="col-4">
                    <form action="actualizar.do" method="POST">
                        <c:forEach var="listaTotal" items="${sessionScope.personas}">
                            <label class="form-label"><strong> DUI:</strong></label>
                            <input type="text" name="txtDui" value="${listaTotal.dui}" readonly class="form-control"/><br><br>

                            <label class="form-label"><strong>Apellidos:</strong></label>
                            <input type="text" name="txtApellidos" value="${listaTotal.apellidos}" class="form-control"/><br><br>

                            <label class="form-label"><strong>Nombres:</strong></label>
                            <input type="text" name="txtNombres" value="${listaTotal.nombres}" class="form-control"/><br><br>
                        </c:forEach>
                        <input type="submit" value="Actualizar Registro" class="btn btn-warning form-control"/>
                    </form>
                    <form action="mostrar.do">
                        <input type="submit" class="btn btn-primary form-control" value="Regresar"/>
                    </form>
                </div>
                <div class="col">
                
                </div>
            </div>
        </div>
        
    </body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>

</html>
